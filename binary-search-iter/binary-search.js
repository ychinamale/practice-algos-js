// Binary Search (Iterative)

function numeric(a,b){
    return a - b;
}

function getComparison(numA, numB) {
    if (numA < numB) return -1;
    else if (numA == numB) return 0;
    else return 1;
}

function binarySearch(item, targetList) {
    var low = 0;
    var high = targetList.length - 1;

    // sort the lists numerically
    targetList = targetList.sort(numeric)

    while (low <= high){
        var mid = Math.floor( (low + high)/2 )
        var comp = getComparison(item, targetList[mid])

        if (comp < 0) {
            high = mid - 1;
        }
        else if (comp > 0) {
            low = mid + 1;
        }
        else return mid;
    }
    return -1;
}

// array to search
var targetList = [2, 5, 1, 9, 4, 10, 13, 8, 3]

var solution = binarySearch(5, targetList)

console.log("Found at position: " + solution)